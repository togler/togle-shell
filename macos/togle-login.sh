#!/bin/bash
echo -n "username: "
read username
echo -n "password: "
read -s password
echo
echo $password | docker login docker-registry.togle.io -u $username --password-stdin
if [ $? -eq 0 ];then
  mkdir -p ~/.togle
  cat << EOF > ~/.togle/credentials
USERNAME=$(echo $username | base64)
PASSWORD=$(echo $password | base64)
EOF
fi