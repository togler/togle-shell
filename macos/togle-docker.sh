#!/bin/bash
if [ -f ~/.togle/credentials ]
then
  source ~/.togle/credentials
fi
if [ -z $USERNAME ]
then
  echo -n "jenkins’s username: "
  read username
  echo -n "jenkins’s password: "
  read -s password
  echo
else
  username=$(echo "$USERNAME" | base64 --decode)
  password=$(echo "$PASSWORD" | base64 --decode)
fi
curl -u $username:$password -o $PWD/.docker/.dirs http://jenkins/userContent/togle/docker/.dirs
curl -u $username:$password -o $PWD/.docker/.downloads http://jenkins/userContent/togle/docker/.downloads
IFS=$'\n'       # make newlines the only separator
for dir in $(cat $PWD/.docker/.dirs)
do
    mkdir -p $PWD/.docker/$dir
done
for file in $(cat $PWD/.docker/.downloads)
do
    curl -u $username:$password -o $PWD/.docker/$file http://jenkins/userContent/togle/docker/$file
    chmod +x $PWD/.docker/$file
done
for service in $(cat $PWD/.docker/.services)
do
    $PWD/.docker/+.template.sh $service > $PWD/.docker/+$service
    chmod +x $PWD/.docker/+$service
    $PWD/.docker/-.template.sh $service > $PWD/.docker/-$service
    chmod +x $PWD/.docker/-$service
    [ ! -f $PWD/.docker/.env.$service.local ] && touch $PWD/.docker/.env.$service.local
done
[ ! -f $PWD/.docker/.env.local ] && touch $PWD/.docker/.env.local
[ ! -f $PWD/.docker/tags.env.local ] && touch $PWD/.docker/tags.env.local
[ ! -f $PWD/.docker/options.env.local ] && touch $PWD/.docker/options.env.local