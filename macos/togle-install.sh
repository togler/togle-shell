#!/bin/bash

# Usage:
# ./install-togle.sh 실행
#
# 환경변수 설정
# 설치 : 프로젝트 루트(최상위 부모 프로젝트) 에서 togle-env 실행
# 확인 : .env 파일이 생성 되면 정상
# 사용 : .env 파일 내 환경 변수가 java project 실행시 적용됨
#
#
# docker 설정
# 설치 : 프로젝트 루트(최상위 부모 프로젝트) 에서 togle-docker 실행
# 확인 : .docker 폴더가 생성 되면 정상
# 사용 :
# .docker/up : 전체 실행
# .docker/down : 전체 종료
# .docker/pull : 전체 업데이트
# .docker/-서비스명 : 지정 서비스를 제외한 다른 모든 서비스를 실행
# .docker/+서비스명 : 지정 서비스와 의존되는 모든 서비스를 실행
#

curl -o /usr/local/bin/togle-dns https://bitbucket.org/togler/togle-shell/raw/master/macos/togle-dns.sh
curl -o /usr/local/bin/togle-env https://bitbucket.org/togler/togle-shell/raw/master/macos/togle-env.sh
curl -o /usr/local/bin/togle-docker https://bitbucket.org/togler/togle-shell/raw/master/macos/togle-docker.sh
curl -o /usr/local/bin/togle-login https://bitbucket.org/togler/togle-shell/raw/master/macos/togle-login.sh
curl -o /usr/local/bin/togle-waybill-find https://bitbucket.org/togler/togle-shell/raw/master/macos/togle-waybill-find.sh
curl -o /usr/local/bin/auth-log-find https://bitbucket.org/togler/togle-shell/raw/master/macos/auth-log-find.sh
chmod +x /usr/local/bin/togle-dns
chmod +x /usr/local/bin/togle-env
chmod +x /usr/local/bin/togle-docker
chmod +x /usr/local/bin/togle-login
chmod +x /usr/local/bin/togle-waybill-find
chmod +x /usr/local/bin/auth-log-find

# too many open files
cat << EOF >> /Library/LaunchDaemons/limit.maxfiles.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN"
          "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
  <dict>
    <key>Label</key>
    <string>limit.maxfiles</string>
    <key>ProgramArguments</key>
    <array>
      <string>launchctl</string>
      <string>limit</string>
      <string>maxfiles</string>
      <string>524288</string>
      <string>524288</string>
    </array>
    <key>RunAtLoad</key>
    <true/>
    <key>ServiceIPC</key>
    <false/>
  </dict>
</plist>
EOF

cat << EOF >> /Library/LaunchDaemons/limit.maxproc.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple/DTD PLIST 1.0//EN"
          "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
  <dict>
    <key>Label</key>
      <string>limit.maxproc</string>
    <key>ProgramArguments</key>
      <array>
        <string>launchctl</string>
        <string>limit</string>
        <string>maxproc</string>
        <string>2048</string>
        <string>2048</string>
      </array>
    <key>RunAtLoad</key>
      <true />
    <key>ServiceIPC</key>
      <false />
  </dict>
</plist>
EOF

togle-dns