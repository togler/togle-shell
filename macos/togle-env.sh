#!/bin/bash
if [ -f ~/.togle/credentials ]
then
  source ~/.togle/credentials
fi
if [ -z $USERNAME ]
then
  echo -n "jenkins’s username: "
  read username
  echo -n "jenkins’s password: "
  read -s password
  echo
else
  username=$(echo "$USERNAME" | base64 --decode)
  password=$(echo "$PASSWORD" | base64 --decode)
fi
curl -u $username:$password -o $PWD/.env http://jenkins/userContent/togle/.env
[ ! -f $PWD/.env.local ] && touch $PWD/.env.local
