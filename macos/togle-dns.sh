#!/bin/bash
PSID=$( (scutil | grep PrimaryService | sed -e 's/.*PrimaryService : //')<< EOF
get State:/Network/Global/IPv4
d.show
exit
EOF
)
sudo scutil << EOF
get State:/Network/Service/$PSID/DNS
d.remove ServerAddresses
d.add ServerAddresses 61.251.171.99 61.251.171.100
set State:/Network/Service/$PSID/DNS
exit
EOF