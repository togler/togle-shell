# Usage:

## macos
```
sudo bash -c "$(curl https://bitbucket.org/togler/togle-shell/raw/master/macos/togle-install.sh)"
```

#
# 환경변수 설정
# 설치 : 프로젝트 루트(최상위 부모 프로젝트) 에서 togle-env 실행
# 확인 : .env 파일이 생성 되면 정상
# 사용 : .env 파일 내 환경 변수가 java project 실행시 적용됨
#
#
# docker 설정
# 설치 : 프로젝트 루트(최상위 부모 프로젝트) 에서 togle-docker 실행
# 확인 : .docker 폴더가 생성 되면 정상
# 사용 :
# .docker/up : 전체 실행
# .docker/down : 전체 종료
# .docker/pull : 전체 업데이트
# .docker/-서비스명 : 지정 서비스를 제외한 다른 모든 서비스를 실행
# .docker/+서비스명 : 지정 서비스와 의존되는 모든 서비스를 실행
#